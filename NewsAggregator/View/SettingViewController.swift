//
//  SettingViewController.swift
//  NewsAggregator
//
//  Created by Anzhelika on 31.01.2021.
//

protocol UsersData {
    func settingValue(item: String)
    func sourceValues(types: [String: Bool])
}

import UIKit

class SettingViewController: UIViewController {
    
    var published = ["publishedAt", "relevancy", "popularity"]
    var previousSelected : IndexPath?
    var currentSelected : Int?
    
    //Array for checkmark
    var source =  [Items]()
    var globeArray = [ItemsGlobe]()
    var sourceArray = [String]()
    var sourceDict = [String: Bool]()
    
    
    let dataFilePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("Itemsv1.plist")
    let dataFilePathGlobe = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("ItemsGlobev2.plist")
    var usersChoise: String?
    var delegate: UsersData?
    
    @IBOutlet var collectionView: UICollectionView!
    
    
    convenience init(source: [Items]) {
        self.init()
        self.source = source
    }
    
    
    @IBOutlet var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        globeArray = LoadCountry().itemsGlobe
        source = LoadSource().items
        
        loadGlobe()
        loadSource()
        
    }
    
}


//MARK: - UICollectionViewDataSource, UICollectionViewDelegate

extension SettingViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return globeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "go", for: indexPath) as! CountryData
        
        if globeArray[indexPath.row].done == true || (currentSelected != nil && currentSelected == indexPath.row)
        {
            previousSelected = indexPath
            cell.backgroundColor = UIColor.systemGray5
            
        }
        else
        {
            
            cell.backgroundColor = UIColor.white
        }
        
        cell.countryLabel.text = globeArray[indexPath.row].title
        
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if previousSelected != nil{
            if let cell = collectionView.cellForItem(at: previousSelected!){
                cell.backgroundColor = UIColor.white
            }
            globeArray[previousSelected!.row].done = false
            
        }
        currentSelected = indexPath.row
        previousSelected = indexPath
        globeArray[currentSelected!].done = true
        collectionView.reloadItems(at: [indexPath])
        saveGlobe()
        
        if globeArray[currentSelected!].done == true {
            self.delegate?.settingValue(item: globeArray[currentSelected!].title)
            dismiss(animated: true, completion: .none)
        }
    }
    
    
    func saveGlobe() {
        let encoder = PropertyListEncoder()
        do {
            let data = try encoder.encode(self.globeArray)
            
            try data.write(to: self.dataFilePathGlobe!)
        }
        catch {
            print("error encoding source array\(error)")
        }
        self.collectionView.reloadData()
    }
    
    
    
    
    func loadGlobe() {
        if let data = try? Data(contentsOf: dataFilePathGlobe!) {
            let decoder = PropertyListDecoder()
            do {
                
                globeArray = try decoder.decode([ItemsGlobe].self, from: data)
            }
            catch {
                print("Error decoding\(error)")
            }
            
        }
    }
}




//MARK: - UITableViewDelegate, UITableViewDataSource

extension SettingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return source.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSource", for: indexPath)
        cell.textLabel?.text = source[indexPath.row].title
        cell.accessoryType = source[indexPath.row].done ? .checkmark : .none
        
        return cell
    }
    
    func saveSource() {
        let encoder = PropertyListEncoder()
        do {
            let data = try encoder.encode(self.source)
            
            try data.write(to: self.dataFilePath!)
        }
        catch {
            print("error encoding source array\(error)")
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            if cell.accessoryType == .checkmark {
                cell.accessoryType = .none
                source[indexPath.row].done = false
                
                if let index = sourceArray.firstIndex(of: source[indexPath.row].title) {
                    sourceArray.remove(at: index)
                }
                sourceDict.updateValue(false, forKey: source[indexPath.row].title)
                print("source dict without value \(sourceDict)")
                saveSource()
                self.delegate?.sourceValues(types: sourceDict)
                dismiss(animated: true, completion: .none)
            } else {
                cell.accessoryType = .checkmark
                source[indexPath.row].done = true
                sourceArray.append(source[indexPath.row].title)
                sourceDict.updateValue(true, forKey: source[indexPath.row].title)
                print("source dict with value \(sourceDict)")
                saveSource()
                self.delegate?.sourceValues(types: sourceDict)
                dismiss(animated: true, completion: .none)
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
    func loadSource() {
        if let data = try? Data(contentsOf: dataFilePath!) {
            let decoder = PropertyListDecoder()
            do {
                
                source = try decoder.decode([Items].self, from: data)
                
            }
            catch {
                print("Error decoding\(error)")
            }
            
        }
        
    }
    
}

