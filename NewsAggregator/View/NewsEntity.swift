//
//  NewsEntity.swift
//  NewsAggregator
//
//  Created by Anzhelika on 30.01.2021.
//

import UIKit

class NewsEntity: UITableViewCell {
    

    @IBOutlet var imageCell: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var descriptions: UILabel!
    @IBOutlet var source: UILabel!
    @IBOutlet var author: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
