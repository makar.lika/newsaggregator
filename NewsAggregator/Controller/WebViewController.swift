//
//  WebViewController.swift
//  NewsAggregator
//
//  Created by Anzhelika on 30.01.2021.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
    var url: String?
    @IBOutlet var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        super.loadView()
        if webView != nil {
            let urlRequest = URL(string: url!)!
            let request = URLRequest(url: urlRequest)
            webView.load(request)
        }
    }
}
