//
//  ViewController.swift
//  NewsAggregator
//
//  Created by Anzhelika on 30.01.2021.
//

import UIKit

class ViewController: UIViewController  {
    
    var newsManager = NewsManager()
    var newsOutput = [NewsModels]()
    var webVC = WebViewController()
    var selectRowUrl: String?
    var typeCategory = "general"
    var searchWord: String?
    var country = "ua"
    var source = [String: Bool]()
    var published = "publishedAt"
    
    let dataFilePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("Itemsv1.plist")
    
    //PickerView data
    let publishedList = ["publishedAt", "relevancy", "popularity"]
    var pickerView = UIPickerView()
    var pickerViewGlobal = UIPickerView()
    var typeValue: String?
    var globeValue: String?
    var sourceForItems =  [Items]()
    
    
    //Pagination
    var totalResult = 0
    var currentPage = 1
    var pageSize = 10
    var isLoadingList : Bool = false
    
    var previousSelected : IndexPath?
    var currentSelected : Int?
    
    var categoryArray = [ItemsCategory]()
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    var defaults = UserDefaults.standard
    var persistantData = Items()
    var svc = SettingViewController()
    let dataFilePathCategory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("ItemsCategoryv2.plist")
    
    var myRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newsManager.delegate = self
                tableView.register(UINib(nibName: "NewsEntity", bundle:  nil), forCellReuseIdentifier: "EntityCell")
        
        categoryArray = LoadCategory().itemsCategory
        loadCategory()

        loadSource()
        
        newsManager.fetchNews(country: country, category: typeCategory, page: currentPage)
        tableView.refreshControl = myRefreshControl
    }
    
    
    @objc func refresh(sender: UIRefreshControl) {
        isLoadingList = true
        currentPage -= 1
        
        DispatchQueue.main.async {
            self.newsOutput.removeAll()
            self.newsManager.fetchNews(country: self.country, category: self.typeCategory, page: 1)
            self.tableView.reloadData()
        }
        if self.myRefreshControl.isRefreshing == true{
            sender.endRefreshing()
        }
    }
    
    @IBAction func settings(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "settings", sender: self)
    }
    
    
    
    @IBAction func searchBy(_ sender: UIBarButtonItem) {
        
        let alertView = UIAlertController(title: "Sorted, only for search",message: "\n\n\n\n\n\n\n\n\n\n",preferredStyle: .alert)
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 50, width: 260, height: 162))
        
        pickerView.dataSource = self
        pickerView.delegate = self
        pickerView.backgroundColor = UIColor.white
        alertView.view.addSubview(pickerView)
        
        
        let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (action) in
            
            if self.searchWord != nil {
                DispatchQueue.main.async {
                    self.newsOutput.removeAll()
                    self.newsManager.fetchSearch(keyword: self.searchWord!, sortBy: self.published)
                }
                
            } else  if self.searchWord == nil {
                DispatchQueue.main.async {
                    self.newsManager.fetchNews(country: self.country, category: self.typeCategory, page: 1)
                }
            }
        }
        alertView.addAction(action)
        
        present(alertView, animated: true, completion: {
            pickerView.frame.size.width = alertView.view.frame.size.width
        })
    }
    
    func loadSource() {
        if let data = try? Data(contentsOf: dataFilePath!) {
            let decoder = PropertyListDecoder()
            do {
                
                sourceForItems = try decoder.decode([Items].self, from: data)
                for item in 0..<sourceForItems.count {
                    source.updateValue(sourceForItems[item].done, forKey: sourceForItems[item].title)
                }
                print(source)
            }
            catch {
                print("Error decoding\(error)")
            }
            
        }
    }
}

//MARK: - UITableViewDataSource

extension ViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsOutput.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EntityCell", for: indexPath) as! NewsEntity
        cell.title.text = newsOutput[indexPath.row].title
        cell.descriptions.text = newsOutput[indexPath.row].descriptions
        cell.author.text = newsOutput[indexPath.row].athour
        cell.source.text = newsOutput[indexPath.row].source
        
        
        let urlToImage = newsOutput[indexPath.row].urlToImage
        
        if (isValidUrlStr(urlToImage)) {
            if  let urlRequest = URL(string: urlToImage!){
                DispatchQueue.global().async {
                    if let data = try? Data(contentsOf: (urlRequest)) {
                        DispatchQueue.main.async {
                            cell.imageCell.image = UIImage(data: data)
                        }
                    }
                }
            }
        }
        else {
            print("Invalid url detected: \(urlToImage)")
        }
        totalResult = newsOutput[0].totalResults ?? 1
        return cell
    }
    
    func isValidUrlStr(_ string: String?) -> Bool {
        
        if (string == nil) {
            return false
        }
        
        let regEx = "(https|http)://(\\w)+.*"
        
        let predicate = NSPredicate(format:"SELF MATCHES %@", argumentArray:[regEx])
        return predicate.evaluate(with: string)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectRowUrl = newsOutput[indexPath.row].url
        self.performSegue(withIdentifier: "showLink", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as? WebViewController
        destinationVC?.url = selectRowUrl
        
        let destinationSettings = segue.destination as? SettingViewController
        destinationSettings?.delegate = self
        
    }
}

//MARK: - UICollectionViewDataSource

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionData
        if categoryArray[indexPath.row].done == true || (currentSelected != nil && currentSelected == indexPath.row)
        {
            previousSelected = indexPath
            cell.backgroundColor = UIColor.systemGray5
            
        }
        else
        {
            cell.backgroundColor = UIColor.white
        }
        cell.category.text = categoryArray[indexPath.row].title
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        loadSource()
        typeCategory = categoryArray[indexPath.row].title
        
        var sourceUrl = ""
        if typeCategory == "SourceNews" {
            
            for (key, value) in source {
                if value == true {
                    sourceUrl = sourceUrl + "sources=\(key)&"
                }
            }
            
            if sourceUrl != "" {
                sourceUrl.removeLast()
            }
            
            DispatchQueue.main.async { [self] in
                self.newsOutput.removeAll()
                self.newsManager.fetchSource(source: sourceUrl, sorted: self.published)
            }
            
            
            if previousSelected != nil{
                if let cell = collectionView.cellForItem(at: previousSelected!){
                    cell.backgroundColor = UIColor.white
                }
                categoryArray[previousSelected!.row].done = false
            }
            
            currentSelected = indexPath.row
            previousSelected = indexPath
            
            categoryArray[currentSelected!].done = true
            collectionView.reloadItems(at: [indexPath])
            saveCategory()
            
        } else {
            DispatchQueue.main.async { [self] in
                self.newsOutput.removeAll()
                currentPage = 1
                self.newsManager.fetchNews(country: self.country, category: self.typeCategory, page: self.currentPage)
                
            }
            if previousSelected != nil{
                if let cell = collectionView.cellForItem(at: previousSelected!){
                    cell.backgroundColor = UIColor.white
                }
                categoryArray[previousSelected!.row].done = false
                
            }
            
            currentSelected = indexPath.row
            previousSelected = indexPath
            categoryArray[currentSelected!].done = true
            collectionView.reloadItems(at: [indexPath])
            saveCategory()
            
        }
    }
    
    
    func saveCategory() {
        let encoder = PropertyListEncoder()
        do {
            let data = try encoder.encode(self.categoryArray)
            
            try data.write(to: self.dataFilePathCategory!)
        }
        catch {
            print("error encoding source array\(error)")
        }
    }
    
    
    func loadCategory() {
        if let data = try? Data(contentsOf: dataFilePathCategory!) {
            let decoder = PropertyListDecoder()
            do {
                
                categoryArray = try decoder.decode([ItemsCategory].self, from: data)
                for item in 0..<categoryArray.count{
                    if categoryArray[item].done == true{
                        typeCategory = categoryArray[item].title
                    }
                }
            }
            catch {
                print("Error decoding\(error)")
            }
            
        }
    }
}



//MARK: - NewsManagerDelegate

extension ViewController: NewsManagerDelegate {
    
    func updateArray(news: NewsModels) {
        for item in 0..<news.newsToView!.count {
            
            newsOutput.append(NewsModels(totalResults: news.totalResults ?? 1,
                                         title: news.newsToView![item].title,
                                         descriptions: news.newsToView![item].descriptions,
                                         source: news.newsToView![item].source,
                                         athour: news.newsToView?[item].athour ?? "",
                                         urlToImage: news.newsToView![item].urlToImage,
                                         publishedAt: nil,
                                         url: news.newsToView![item].url,
                                         newsToView: nil))
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
}

//MARK: - UISearchBarDelegate

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchWord = searchText
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        DispatchQueue.main.async {
            self.newsOutput.removeAll()
            self.newsManager.fetchSearch(keyword: self.searchWord!, sortBy: self.published)
            searchBar.text = ""
            self.tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
}

//MARK: - Usersdata
extension ViewController: UsersData {
    func relevantList(type: String) {
        published = type
    }
    
    func sourceValues(types: [String: Bool]) {
        source = types
    }
    
    func settingValue(item: String) {
        country = item
    }
    
}

//MARK: - UIPickerViewDelegate, UIPickerViewDataSource

extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return publishedList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return publishedList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        typeValue = publishedList[row]
        published = typeValue!
        print(" from picker \(published)")
    }
}

//MARK: - UIScrollViewDelegate

extension ViewController: UIScrollViewDelegate {
    
    func countPage()-> Int {
        let value: CGFloat = CGFloat(totalResult) / CGFloat(pageSize)
        let limitPage = ceil(value)
        return Int(limitPage)
    }
    
    func getListFromServer(_ pageNumber: Int){
        self.isLoadingList = false
        let dataOnPages = (pageSize * currentPage)
        if dataOnPages <= totalResult {
            DispatchQueue.main.async {
                self.newsManager.fetchNews(country: self.country, category: self.typeCategory, page: pageNumber)
                self.tableView.reloadData()
            }
            
        }
    }
    func loadMoreItemsForList(){
        let limit = countPage()
        if currentPage <= limit {
            currentPage += 1
            getListFromServer(currentPage)
        }
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
            self.isLoadingList = true
            self.loadMoreItemsForList()
        }
    }
    
}

