//
//  Items.swift
//  NewsAggregator
//
//  Created by Anzhelika on 04.02.2021.
//

import Foundation

struct Items: Codable {
    var title: String = ""
    var done: Bool = false
}

