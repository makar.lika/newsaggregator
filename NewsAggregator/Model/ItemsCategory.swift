//
//  ItemsCategory.swift
//  NewsAggregator
//
//  Created by Anzhelika on 08.02.2021.
//

import Foundation

struct ItemsCategory: Codable {
    var title: String = ""
    var done: Bool = false
}
