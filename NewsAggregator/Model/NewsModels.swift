//
//  NewsModels.swift
//  NewsAggregator
//
//  Created by Anzhelika on 30.01.2021.
//

import UIKit


struct NewsModels {
    
    var totalResults: Int?
    var title: String?
    var descriptions: String?
    var source: String?
    var athour: String?
    var urlToImage: String?
    var publishedAt: Data?
    var url: String?
    let newsToView: [NewsModels]?
    
}
