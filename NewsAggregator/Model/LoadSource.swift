//
//  LoadSource.swift
//  NewsAggregator
//
//  Created by Anzhelika on 08.02.2021.
//

import Foundation

public class LoadSource {
    @Published var items = [Items]()
    var source = Items()
    
    init() {
        load()
    }
    
    func load() {
        do {
            if let path = Bundle.main.path(forResource: "source", ofType: "txt"){
                let data = try String(contentsOfFile:path, encoding: String.Encoding.utf8)
                let  text = data.components(separatedBy: "\n")
                for item in 0..<text.count-1{
                    source.title = text[item]
                    items.append(source)
                }
            }
        } catch let error {
            Swift.print("Fatal Error: \(error.localizedDescription)")
        }
    }
}
