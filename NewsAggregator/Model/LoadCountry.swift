//
//  LoadCountry.swift
//  NewsAggregator
//
//  Created by Anzhelika on 07.02.2021.
//

import Foundation

public class LoadCountry {
    @Published var itemsGlobe = [ItemsGlobe]()
    var globe = ItemsGlobe()
    
    init() {
        load()
    }
    
    func load() {
        do {
            if let path = Bundle.main.path(forResource: "country", ofType: "txt"){
                let data = try String(contentsOfFile:path, encoding: String.Encoding.utf8)
                let  text = data.components(separatedBy: "\n")
                for item in 0..<text.count-1{
                    globe.title = text[item]
                    itemsGlobe.append(globe)
                }
            }
        } catch let error {
            Swift.print("Fatal Error: \(error.localizedDescription)")
        }
    }
}
