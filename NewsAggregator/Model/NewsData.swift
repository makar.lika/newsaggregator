//
//  NewsData.swift
//  NewsAggregator
//
//  Created by Anzhelika on 30.01.2021.
//

import UIKit

struct NewsData: Codable {
    let totalResults: Int
    let articles: [Article]
}

struct Article: Codable {
    let source: Source
    let author, title, articleDescription: String?
    let url, urlToImage: String?
    let publishedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case source, author, title
        case articleDescription = "description"
        case url, urlToImage, publishedAt
    }
}

struct Source: Codable {
    let name: String?
}


