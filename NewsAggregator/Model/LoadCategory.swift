//
//  LoadCategory.swift
//  NewsAggregator
//
//  Created by Anzhelika on 08.02.2021.
//

import Foundation

public class LoadCategory {
    @Published var itemsCategory = [ItemsCategory]()
    var category = ItemsCategory()
    
    init() {
        load()
    }
    
    func load() {
        do {
            if let path = Bundle.main.path(forResource: "category", ofType: "txt"){
                let data = try String(contentsOfFile:path, encoding: String.Encoding.utf8)
                let  text = data.components(separatedBy: "\n")
                for item in 0..<text.count-1{
                    category.title = text[item]
                    itemsCategory.append(category)
                }
            }
        } catch let error {
            Swift.print("Fatal Error: \(error.localizedDescription)")
        }
    }
}
