//
//  ItemsGlobe.swift
//  NewsAggregator
//
//  Created by Anzhelika on 05.02.2021.
//

import Foundation

struct ItemsGlobe: Codable {
    var title: String = ""
    var done: Bool = false
}
