//
//  NewsManager.swift
//  NewsAggregator
//
//  Created by Anzhelika on 30.01.2021.
//

import UIKit

protocol NewsManagerDelegate {
    func updateArray(news: NewsModels)
    func didFailWithError(error: Error)
}

struct NewsManager {
    let apiKey = "5ada7b506dec4699b757fc3a9c138ca4"
    let newsURL = "https://newsapi.org/v2/top-headlines?"
    var delegate: NewsManagerDelegate?
    
    
    func fetchNews(country: String, category: String, page: Int)  {
        
        let urlString = "\(newsURL)pageSize=10&page=\(page)&country=\(country)&category=\(category)&apiKey=\(apiKey)"
        performRequest(with: urlString)
    }
    
    func fetchSource(source: String, sorted: String){
        let urlString =  "https://newsapi.org/v2/top-headlines?\(source)&sortBy=\(sorted)&apiKey=\(apiKey)"
        performRequest(with: urlString)
    }
    
    func fetchSearch(keyword: String, sortBy: String)  {
        let urlString = "https://newsapi.org/v2/top-headlines?q=\(keyword)&sortBy=\(sortBy)&apiKey=\(apiKey)"
        performRequest(with: urlString)
    }
    
    
    func performRequest(with urlString: String) {
        if (urlString != nil){
            let urlEscaped = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            let url = URL(string: urlEscaped!)
            if (url != nil) {
                let session = URLSession(configuration: .default)
                let task = session.dataTask(with: url!) { (data, response, error) in
                    if error != nil {
                        self.delegate?.didFailWithError(error: error!)
                        return
                    }
                    if let safeData = data {
                        if let news = self.parseJSonSecond(safeData)  {
                            //self.delegate?.didUpdateNews(self, news: news)
                            self.delegate?.updateArray(news: news)
                        }
                    }
                }
                task.resume()
            }
        }
    }
    
    
    func parseJSonSecond(_ newsData: Data) -> NewsModels? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(NewsData.self, from: newsData)
            
            var currentNews = [NewsModels]()
            
            for item in decodedData.articles {
                
                let newsEntry = NewsModels(totalResults: decodedData.totalResults ?? 1,
                                          title: item.title ?? "",
                                          descriptions: item.articleDescription ?? "",
                                          source: item.source.name ?? "bbc-news",
                                          athour: item.author ?? "",
                                          urlToImage: item.urlToImage,
                                          url: item.url ?? "https://www.google.com.ua",
                                          newsToView: nil)
                currentNews.append(newsEntry)
            }

            return NewsModels(totalResults: decodedData.totalResults ?? 1, title: "", descriptions: "", source: "", athour: "", urlToImage:"", publishedAt: nil, url: "", newsToView: currentNews)
        
        } catch {
            delegate?.didFailWithError(error: error)
            return nil
        }
    }
}
