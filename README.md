
The ios mobile app fetches the news from newsapi.org.


## Main features:
Available filters:
  - by news category;
  - by country;
  - by source;

Allows sorting by publish date, relevance and popularity.

Search by phrase in the news is implemented.

Pagination is implemented that allows user to fetch more data if available.

User can refresh the screen to get latest fresh news.

The API is hardcoded and should be changed if new one is required.

Please use to get new api key:
https://newsapi.org/register

and update var apiKey in class NewsManager.swift

Screens of the app:
![Application views](Documentation/main.png)
